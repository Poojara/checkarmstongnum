import UIKit



func arm(_num : Int) -> Bool{
    
    var num = String(_num)
        
    var strArr = Array(num)
    debugPrint(strArr)
    
    var numArr = strArr.map { c in
        c.hexDigitValue
    }

    debugPrint(numArr)
    
    var cal = numArr.reduce(0) { parRes, number in
        let num = (number ?? 0)  * (number ?? 0) * (number ?? 0)
      return  parRes + num
    }
    debugPrint(cal)

    
        return (cal == _num) ? true : false
    
}

func armWithoutHigerOrder(_num : Int) -> Bool{
    
    var num = String(_num)
    var numArr = [Int]()
        
    var strArr = Array(num)
    debugPrint(strArr)

    for s in strArr {
        let reNum : Int?  = s.hexDigitValue ?? 0
        numArr.append(reNum ?? 0)
    }

    debugPrint(numArr)

    var cal = 0
    for n in numArr{

        cal = cal + (n*n*n)

    }
    print(cal)
    return (cal == _num) ? true : false
    
}


debugPrint(arm(_num : 153))



//
//var fn  = 4
//var sn = 5
//
//fn = fn + sn
//sn = fn - sn
//fn = fn - sn
//
//debugPrint("a = \(fn) , b = \(sn)")
//
